package com.meseems.mereach

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.util.Log
import com.meseems.mereach.models.Server
import com.meseems.mereach.storage.IStorageManager
import com.meseems.mereach.storage.impl.PreferenceStorageManager
import com.meseems.mereach.ui.mainscreen.ServersAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.onRefresh
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    var mAdapter: ServersAdapter? = null
    var mLinearLayoutManager: LinearLayoutManager? = null
    var servers: List<Server>? = null
    var timer: Observable<Long>? = null
    var storageManager: IStorageManager? = null

    // Utility methods
    fun initVariables() {
        mLinearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLinearLayoutManager
        storageManager = PreferenceStorageManager(baseContext)
    }
    fun initListeners() {
        fab.onClick { onAddClick() }
        swiperefresh.onRefresh { onSwipeRefresh() }
        timer = Observable.interval(30, TimeUnit.SECONDS)
    }
    fun initData() {
        val googleServer = Server("www.google.com.br", "Offline")
        val meseemsServer = Server("www.meseems.com.br", "Offline")
        val otherServer = Server("www.doesnt-exist.com.br", "Offline")
        this.servers = listOf(googleServer, meseemsServer, otherServer)
        mAdapter = ServersAdapter(servers as List<Server>)
        recyclerView.adapter = mAdapter
    }
    fun initRx() {
        val timerSubscription = timer?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
        timerSubscription?.subscribe(
                { next ->
                    Log.d("TimerSubscription", "Next")
                    mAdapter?.refreshData(mAdapter!!.servers)
                    storageManager?.saveServers(mAdapter!!.servers)
                },
                { error ->
                    Log.d("TimerSubscription", "Error")
                },
                {
                    Log.d("TimerSubscription", "Completed")
                }
        )
    }
    fun updateServerList(serverName: Editable) {
        val server = Server(serverName.toString(), "Offline")
        mAdapter!!.servers = mAdapter!!.servers.plus(server)
        mAdapter?.refreshData(mAdapter!!.servers)
    }

    // Listeners
    fun onAddClick() {
        alert {
            customView {
                verticalLayout {
                    padding = 20
                    val serverName = editText {
                        hint = "Server url"
                    }
                    positiveButton("Add") {
                        updateServerList(serverName.text)
                    }
                    negativeButton("Cancel") { }
                }
            }
        }.show()
    }
    fun onSwipeRefresh() {
        mAdapter?.refreshData(mAdapter!!.servers)
        swiperefresh.isRefreshing = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.initVariables()
        this.initListeners()
        this.initData()

        // InitRx needs go last since it uses data from previous initializations
        this.initRx()
    }
}
