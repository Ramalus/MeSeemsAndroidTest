package com.meseems.mereach.networking.impl

import android.util.Log
import com.meseems.mereach.networking.IReachabilityService
import rx.Observable
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

/**
 * Created by nickmm on 8/23/16.
 */
class ReachabilityService : IReachabilityService {
    val TAG: String = this.javaClass.name

    override fun isReachable(serverUrl: String): Observable<Boolean> {

        // Using Observable.create
        // https://github.com/ReactiveX/RxJava/wiki/Creating-Observables

        return Observable.create {
            subscriber ->
                // Method called when subscribed
                // onNext could be called inside an asynchronous method too.
                subscriber.onNext(checkReachability(serverUrl, 1000))
                subscriber.onCompleted()
        }
    }

    private fun checkReachability(url: String, timeout: Int): Boolean {
        // What about an implementation using sockets?
        // Using example in: http://stackoverflow.com/questions/9922543/why-does-inetaddress-isreachable-return-false-when-i-can-ping-the-ip-address

        try {
            val soc = Socket()
            soc.connect(InetSocketAddress(url, 80), timeout)
            return true
        } catch (ex: IOException) {
            Log.d(TAG, ex.message)
            return false
        }
    }
}
