package com.meseems.mereach.storage.impl

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.google.gson.Gson
import com.meseems.mereach.models.Server
import com.meseems.mereach.storage.IStorageManager


/**
 * Created by ramalus on 10/19/16.
 */

class PreferenceStorageManager(val baseContext: Context) : IStorageManager {
    override fun retrieveServers(): List<Server> {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun saveServers(servers: List<Server>): Boolean {
        val gson = Gson()
        val json = gson.toJson(servers)

        try {
            val prefs = PreferenceManager.getDefaultSharedPreferences(baseContext)
            val editor = prefs.edit()
            val servers = prefs?.getString("servers", "").toString()
            // TODO: try to convert do json object?

            editor.putString("servers", json.toString())
            editor.apply()
            return true
        } catch (ex: Exception) {
            Log.e(this.javaClass.name, ex.message)
            return false
        }
    }
}