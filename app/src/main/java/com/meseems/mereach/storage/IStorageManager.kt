package com.meseems.mereach.storage

import com.meseems.mereach.models.Server

/**
 * Created by ramalus on 10/19/16.
 */

interface IStorageManager {
    fun saveServers(servers: List<Server>): Boolean
    fun retrieveServers(): List<Server>
}