package com.meseems.mereach.ui.mainscreen

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.meseems.mereach.R
import com.meseems.mereach.models.Server
import com.meseems.mereach.networking.impl.ReachabilityService
import kotlinx.android.synthetic.main.server_item_row.view.*
import org.jetbrains.anko.textColor
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ramalus on 10/17/16.
 */


class ServersAdapter(var servers: List<Server>) : RecyclerView.Adapter<ServersAdapter.ServerHolder>() {

    var subscription: Subscription? = null
    private var mRecyclerView: RecyclerView? = null


    inner class ServerHolder(v: View, val adapter: ServersAdapter) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private val txtName: TextView
        private val txtStatus: TextView
        private val txtLastUpdated: TextView
        var mServer: Server? = null
        var servers: List<Server>? = null

        var date: Date? = null
        val dateFormat = SimpleDateFormat("dd/M/yyyy hh:mm:ss") //"HH:mm:ss a"


        init {
            txtName = v.name
            txtStatus = v.status
            txtLastUpdated = v.lastupdated
            v.setOnClickListener(this)

        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")
        }

        fun bindServer(server: Server): Subscription {
            mServer = server
            txtName.text = server.name
            val subscription = ReachabilityService().isReachable(server.name)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        isReachable ->
                        date = Date()
                        val formattedDate = dateFormat.format(date)
                        if (isReachable) {
                            server.status = "Online"
                            txtStatus.textColor = Color.BLUE
                        } else {
                            server.status = "Offline"
                            txtStatus.textColor = Color.RED
                        }
                        txtStatus.text = server.status
                        txtLastUpdated.text = formattedDate
                        servers = servers?.plus(server)
                    }
            return subscription
        }
    }


    fun refreshData(serverList: List<Server>) {
        this.servers = serverList
        notifyDataSetChanged()
    }

    override fun onViewRecycled(holder: ServerHolder?) {
        super.onViewRecycled(holder)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ServerHolder {
        val inflatedView = LayoutInflater.from(parent?.context).inflate(R.layout.server_item_row, parent, false)
        return ServerHolder(inflatedView, this)
    }

    override fun getItemCount(): Int {
        return servers.size
    }

    override fun onBindViewHolder(holder: ServerHolder?, position: Int) {
        val itemServer = servers[position]
        this.subscription = holder?.bindServer(itemServer)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        val itemTouchHelper = ItemTouchHelper(TripItemTouchHelperCallback())
        itemTouchHelper.attachToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }
}

class TripItemTouchHelperCallback : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {

    override fun onMove(recyclerView: RecyclerView,
                        viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
        val serverHolder = (viewHolder as ServersAdapter.ServerHolder)
        val servers = serverHolder.adapter.servers.minus(serverHolder.mServer as Server)
        serverHolder.adapter.refreshData(servers)
    }
}